FROM node:11.9.0-alpine
ENV NODE_ENV production
WORKDIR /usr/src/app
RUN npm install --quiet 
EXPOSE 8000
COPY . .
